﻿using UnityEngine;
using System.Collections;

public class carController : MonoBehaviour {
    private Rigidbody rigibody;
    private float speed;
    private float forwardSpeed;
    private float rightSpeed;


    public float AccelerationForce = 50f;
    public float TorqueForce = 20f;
    public float BreakForce = 50f;
    public float DownForce = -100f;
    public AudioSource AccelerationSound, DecelerationSound;


	void Awake()
    {
        rigibody = GetComponent<Rigidbody>();
    }
	void Start () {
	}

    void FixedUpdate()
    {
        speed = rigibody.velocity.magnitude; //общая скорость
        forwardSpeed = Vector3.Dot(rigibody.velocity, transform.forward); //фронтальная скорость
        rightSpeed = Vector3.Dot(rigibody.velocity, transform.right); //боковая скорость

        rigibody.AddForce(0, 1 / (40 / speed) * DownForce, 0, ForceMode.Acceleration); //прижимаем машину к земле для равновесия

        AccelerationSound.volume = 1 / (40 / speed); //громкость звука зависит от скорости
        DecelerationSound.volume = 1 / (40 / speed);

        Move(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), Input.GetKey(KeyCode.Space)); 
    }




    void Move(float vertical, float horizontal, bool wantToBreak)
    {
        rigibody.AddRelativeForce(0, 0, vertical * AccelerationForce, ForceMode.Acceleration); //ускорение
        rigibody.AddRelativeTorque(0, horizontal * TorqueForce, 0, ForceMode.Acceleration); //поворот

        if (wantToBreak)
        {
            DecelerationPlay();
            float forwardBreak = 1 / (Mathf.Abs(forwardSpeed) + Mathf.Abs(rightSpeed)) * -forwardSpeed * BreakForce; // тут высчитываем с какой силой будем тормозить
            float rightBreak = 1 / (Mathf.Abs(forwardSpeed) + Mathf.Abs(rightSpeed)) * -rightSpeed * BreakForce;
            rigibody.AddRelativeForce(rightBreak, 0, forwardBreak, ForceMode.Acceleration); //тормозим
        }

        //работа со звуком
        if (vertical != 0)
        {
            if (vertical / forwardSpeed > 0) //проверяю совпадает ли ускорение с направлением движения.
                AccelerationPlay();
            else
                DecelerationPlay();

        }
        else
        {
            AccelerationSound.mute = true;
        }

        
    }

    void AccelerationPlay() //для удобства
    {
        if (!AccelerationSound.mute) return;
        AccelerationSound.mute = false;
        DecelerationSound.mute = true;
    }

    void DecelerationPlay()
    {
        if (!DecelerationSound.mute) return;
        AccelerationSound.mute = true;
        DecelerationSound.mute = false;
    }
}
